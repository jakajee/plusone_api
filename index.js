const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const routes = require('./app/routes');
const cors = require('cors');
const errorHandler = require('./app/core/error_middleware');
const authenMiddleware = require('./app/core/authen_middleware');
const path = require('path');

require('./app/core/date_extension')();

// Init express
const app = express();

// middlewares
app.use(logger('dev'));

// cors config
app.use(cors({
    origin: (process.env.NODE_ENV || 'development') === 'development' ? 'http://localhost:8080' : 'http://localhost',
    methods: 'GET,POST,PUT,PATCH,DELETE',
    optionsSuccessStatus: 204
}));

app.use(express.static(path.join(__dirname, 'client')));

// parse incoming request data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.get('/favicon.ico', (req, res) => res.sendStatus(204));

// client app page
app.get(/\/app/, (req, res) => 
    res.sendFile(path.join(__dirname, "client", "app.html"))
);

// client admin page
app.get(/\/admin/, (req, res) => {
    res.sendFile(path.join(__dirname, "client", "admin.html"))
})

// authen middleware
app.use(authenMiddleware);

// routes
routes(app);

app.use(errorHandler);

// client splash page
app.get("*", (req, res) => res.sendFile(path.join(__dirname, "client", "index.html")));


module.exports = app;