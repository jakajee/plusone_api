/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const tblTransactionReview = sequelize.define('tblTransactionReview', {
		uuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			defaultValue: sequelize.fn('uuid_generate_v4'),
			primaryKey: true,
			field: 'uuid'
		},
		reviewRating: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'review_rating'
		},
		reviewDescription: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'review_description'
		},
		userCreated: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_created'
		},
		dateTimeCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_created'
		},
		dateTimeModified: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_modified'
		},
		masterUserUuid: {
			type: DataTypes.UUIDV4,
			allowNull: true,
			references: {
				model: 'tbl_master_user',
				key: 'uuid'
			},
			field: 'master_user_uuid'
		},
		masterCourtUuid: {
			type: DataTypes.UUIDV4,
			allowNull: true,
			references: {
				model: 'tbl_master_court',
				key: 'uuid'
			},
			field: 'master_court_uuid'
		},
		userModified: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_modified'
		},
		reviewDate: {
			type: DataTypes.DATEONLY,
			allowNull: true,
			field: 'review_date'
		}
	}, {
		tableName: 'tbl_transaction_review'
	});

	tblTransactionReview.associate = (models) => {
		tblTransactionReview.belongsTo(models.tblMasterCourt, {
			foreignKey: 'masterCourtUuid'
		});

		tblTransactionReview.belongsTo(models.tblMasterUser, {
			foreignKey: 'masterUserUuid'
		})
	}
	
	return tblTransactionReview;
};
