/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const tblMasterEvent = sequelize.define('tblMasterEvent', {
		uuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			defaultValue: sequelize.fn('uuid_generate_v4'),
			primaryKey: true,
			field: 'uuid'
		},
		eventId: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'event_id'
		},
		eventName: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'event_name'
		},
		eventDescription: {
			type: DataTypes.TEXT,
			allowNull: true,
			field: 'event_description'
		},
		eventReward: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'event_reward'
		},
		eventFee: {
			type: DataTypes.DOUBLE,
			allowNull: true,
			field: 'event_fee'
		},
		eventDateTimeStart: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'event_date_time_start'
		},
		eventDateTimeEnd: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'event_date_time_end'
		},
		eventImg: {
			type: DataTypes.JSON,
			allowNull: true,
			field: 'event_img'
		},
		flagDisabled: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: false,
			field: 'flag_disabled'
		},
		userCreated: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_created'
		},
		userModified: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_modified'
		},
		dateTimeCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_created'
		},
		dateTimeModified: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_modified'
		},
		masterSportTypeUuid: {
			type: DataTypes.UUIDV4,
			allowNull: true,
			references: {
				model: 'tbl_master_sport_type',
				key: 'uuid'
			},
			field: 'master_sport_type_uuid'
		},
		masterCourtUuid: {
			type: DataTypes.UUIDV4,
			allowNull: true,
			references: {
				model: 'tbl_master_court',
				key: 'uuid'
			},
			field: 'master_court_uuid'
		}
	}, {
		tableName: 'tbl_master_event'
	});

	tblMasterEvent.associate = models => {
		tblMasterEvent.belongsTo(models.tblMasterSportType, {
			foreignKey: 'masterSportTypeUuid'
		})

		tblMasterEvent.belongsTo(models.tblMasterCourt, {
			foreignKey: 'masterCourtUuid'
		})
	}

	return tblMasterEvent;
};
