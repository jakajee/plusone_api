/* jshint indent: 1 */

module.exports = function (sequelize, DataTypes) {
	const tblMasterCourt = sequelize.define('tblMasterCourt', {
		uuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			defaultValue: sequelize.fn('uuid_generate_v4'),
			primaryKey: true,
			field: 'uuid'
		},
		courtName: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'court_name'
		},
		courtDescription: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'court_description'
		},
		courtLat: {
			type: DataTypes.DOUBLE,
			allowNull: true,
			field: 'court_lat'
		},
		courtLng: {
			type: DataTypes.DOUBLE,
			allowNull: true,
			field: 'court_lng'
		},
		courtImageList: {
			type: DataTypes.JSONB,
			allowNull: true,
			field: 'court_image_list'
		},
		flagDisabled: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: false,
			field: 'flag_disabled'
		},
		userCreated: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_created'
		},
		userModified: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_modified'
		},
		dateTimeCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_created'
		},
		dateTimeModified: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_modified'
		},
		systemProvinceId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			references: {
				model: 'tbl_system_province',
				key: 'province_id'
			},
			field: 'system_province_id'
		},
		systemAreaId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			references: {
				model: 'tbl_system_area',
				key: 'area_id'
			},
			field: 'system_area_id'
		},
		masterSportTypeUuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			references: {
				model: 'tbl_master_sport_type',
				key: 'uuid'
			},
			field: 'master_sport_type_uuid'
		},
		masterPlaceUuid: {
			type: DataTypes.UUIDV4,
			allowNull: true,
			references: {
				model: 'tbl_master_place',
				key: 'uuid'
			},
			field: 'master_place_uuid'
		},
		courtOpenHours: {
			type: DataTypes.TIME,
			allowNull: true,
			field: 'court_open_hours'
		},
		courtCloseHours: {
			type: DataTypes.TIME,
			allowNull: true,
			field: 'court_close_hours'
		},
		courtCount: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'court_count'
		}
	}, {
			tableName: 'tbl_master_court'
		});

	tblMasterCourt.associate = (models) => {
		tblMasterCourt.belongsTo(models.tblSystemProvince, {
			foreignKey: 'systemProvinceId'
		});

		tblMasterCourt.belongsTo(models.tblSystemArea, {
			foreignKey: 'systemAreaId'
		});

		tblMasterCourt.belongsTo(models.tblMasterSportType, {
			foreignKey: 'masterSportTypeUuid'
		});

		tblMasterCourt.belongsTo(models.tblMasterPlace, {
			foreignKey: 'masterPlaceUuid'
		});

		tblMasterCourt.belongsToMany(models.tblMasterFacility, {
			through: models.tblMasterCourtFacility,
			foreignKey: 'masterCourtUuid',
			otherKey: 'masterFacilityUuid'
		});

		tblMasterCourt.hasMany(models.tblTransactionMatch, {
			foreignKey: 'masterCourtUuid'
		});

		tblMasterCourt.hasMany(models.tblTransactionReview, {
			foreignKey: 'masterCourtUuid'
		});

	}

	tblMasterCourt.spSearchCourt = async (params) => {
		const result = await sequelize.query(
			'select * From "@plusone_sp_search_court"($1, $2)',
			{
				bind: [...params],
				type: sequelize.QueryTypes.SELECT
			}
		);

		return result;
	}

	tblMasterCourt.prototype.getCourtRating = function () {
		const reviewList = this['tblTransactionReviews'];
		if (reviewList && reviewList.length > 0) {
			return reviewList.reduce((rating, reviewModel) => rating + reviewModel.reviewRating, 0) / reviewList.length;
		}

		return 0;
	}

	return tblMasterCourt;
};


