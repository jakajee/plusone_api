/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblSystemUserStatus', {
		userStatusId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'user_status_id'
		},
		userStatusName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_status_name'
		}
	}, {
		tableName: 'tbl_system_user_status'
	});
};
