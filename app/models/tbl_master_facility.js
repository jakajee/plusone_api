/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblMasterFacility', {
		uuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			defaultValue: sequelize.fn('uuid_generate_v4'),
			primaryKey: true,
			field: 'uuid'
		},
		facilityName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'facility_name'
		},
		facilityIcon: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'facility_icon'
		},
		userCreated: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_created'
		},
		userModified: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_modified'
		},
		dateTimeCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_created'
		},
		dateTimeModified: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_modified'
		},
		flagDisabled: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: false,
			field: 'flag_disabled'
		}
	}, {
		tableName: 'tbl_master_facility'
	});
};
