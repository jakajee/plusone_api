/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblMasterPlace', {
		uuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			defaultValue: sequelize.fn('uuid_generate_v4'),
			primaryKey: true,
			field: 'uuid'
		},
		placeName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'place_name'
		},
		placeLat: {
			type: DataTypes.DOUBLE,
			allowNull: true,
			field: 'place_lat'
		},
		placeLng: {
			type: DataTypes.DOUBLE,
			allowNull: true,
			field: 'place_lng'
		},
		placeDescription: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'place_description'
		},
		flagDisabled: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: false,
			field: 'flag_disabled'
		},
		userCreated: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_created'
		},
		userModified: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_modified'
		},
		dateTimeCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_created'
		},
		dateTimeModified: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_modified'
		}
	}, {
		tableName: 'tbl_master_place'
	});
};
