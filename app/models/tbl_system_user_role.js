/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblSystemUserRole', {
		roleId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'role_id'
		},
		roleName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'role_name'
		}
	}, {
		tableName: 'tbl_system_user_role'
	});
};
