/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblMasterSportType', {
		uuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			defaultValue: sequelize.fn('uuid_generate_v4'),
			primaryKey: true,
			field: 'uuid'
		},
		sportName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'sport_name'
		},
		userCreated: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_created'
		},
		userModified: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_modified'
		},
		dateTimeCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_created'
		},
		dateTimeModified: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_modified'
		},
		imgUrl: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'img_url'
		},
		cssIcon: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'css_icon'
		},
		sportId: {
			type: DataTypes.STRING,
			allowNull: false,
			field: 'sport_id'
		}
	}, {
		tableName: 'tbl_master_sport_type'
	});
};
