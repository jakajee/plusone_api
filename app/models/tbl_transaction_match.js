/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const tblTransactionMatch = sequelize.define('tblTransactionMatch', {
		uuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			defaultValue: sequelize.fn('uuid_generate_v4'),
			primaryKey: true,
			field: 'uuid'
		},
		masterCourtUuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			references: {
				model: 'tbl_master_court',
				key: 'uuid'
			},
			field: 'master_court_uuid'
		},
		matchName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'match_name'
		},
		matchCapability: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'match_capability'
		},
		matchTimeStart: {
			type: DataTypes.TIME,
			allowNull: true,
			field: 'match_time_start'
		},
		matchTimeEnd: {
			type: DataTypes.TIME,
			allowNull: true,
			field: 'match_time_end'
		},
		userCreated: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_created'
		},
		userModified: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_modified'
		},
		dateTimeCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_created'
		},
		dateTimeModified: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_modified'
		},
		matchDescription: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'match_description'
		},
		masterEventUuid: {
			type: DataTypes.UUIDV4,
			allowNull: true,
			references: {
				model: 'tbl_master_event',
				key: 'uuid'
			},
			field: 'master_event_uuid'
		}
	}, {
		tableName: 'tbl_transaction_match'
	});

	tblTransactionMatch.associate = (models) => {
		tblTransactionMatch.belongsTo(models.tblMasterCourt, {
			foreignKey: 'masterCourtUuid'
		});

		tblTransactionMatch.belongsTo(models.tblMasterEvent, {
			foreignKey: 'masterEventUuid'
		})

		tblTransactionMatch.hasMany(models.tblTransactionMatchUser, {
			foreignKey: 'transactionMatchUuid',
			sourceKey: 'uuid'
		})
	}

	return tblTransactionMatch;
};

