/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const tblSystemArea = sequelize.define('tblSystemArea', {
		areaId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'area_id'
		},
		areaName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'area_name'
		},
		areaLat: {
			type: DataTypes.DOUBLE,
			allowNull: true,
			field: 'area_lat'
		},
		areaLng: {
			type: DataTypes.DOUBLE,
			allowNull: true,
			field: 'area_lng'
		},
		systemProvinceId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'tbl_system_province',
				key: 'province_id'
			},
			field: 'system_province_id'
		}
	}, {
		tableName: 'tbl_system_area'
	});

	tblSystemArea.associate = (models) => {
		tblSystemArea.belongsTo(models.tblSystemProvince, {
			foreignKey: 'systemProvinceId'
		})
	}

	return tblSystemArea;
};
