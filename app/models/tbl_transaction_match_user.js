/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	const tblTransactionMatchUser = sequelize.define('tblTransactionMatchUser', {
		uuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			defaultValue: sequelize.fn('uuid_generate_v4'),
			primaryKey: true,
			field: 'uuid'
		},
		transactionMatchUuid: {
			type: DataTypes.UUIDV4,
			allowNull: true,
			references: {
				model: 'tbl_transaction_match',
				key: 'uuid'
			},
			field: 'transaction_match_uuid'
		},
		dateTimeCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_created'
		},
		masterUserUuid: {
			type: DataTypes.UUIDV4,
			allowNull: true,
			references: {
				model: 'tbl_master_user',
				key: 'uuid'
			},
			field: 'master_user_uuid'
		}
	}, {
		tableName: 'tbl_transaction_match_user'
	});

	tblTransactionMatchUser.associate = (models) => {
		tblTransactionMatchUser.belongsTo(models.tblMasterUser, {
			foreignKey: 'masterUserUuid'
		})
	}

	// tblTransactionMatchUser.associate = (models) => {
	// 	tblTransactionMatchUser.belongsTo(models.tblTransactionMatch, {
	// 		foreignKey: 'transactionMatchUuid'
	// 	});
	// }
	
	return tblTransactionMatchUser;
};
