/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblSystemProvince', {
		provinceId: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			field: 'province_id'
		},
		provinceName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'province_name'
		},
		provinceLat: {
			type: DataTypes.DOUBLE,
			allowNull: true,
			field: 'province_lat'
		},
		provinceLng: {
			type: DataTypes.DOUBLE,
			allowNull: true,
			field: 'province_lng'
		}
	}, {
		tableName: 'tbl_system_province'
	});
};
