/* jshint indent: 1 */

const asyncHelper = require('../core/async_helper')

module.exports = function(sequelize, DataTypes) {
	const tblMasterUser = sequelize.define('tblMasterUser', {
		uuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			defaultValue: sequelize.fn('uuid_generate_v4'),
			primaryKey: true,
			field: 'uuid'
		},
		systemUserRoleId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'tbl_system_user_role',
				key: 'role_id'
			},
			field: 'system_user_role_id'
		},
		systemUserStatusId: {
			type: DataTypes.INTEGER,
			allowNull: true,
			references: {
				model: 'tbl_system_user_status',
				key: 'user_status_id'
			},
			field: 'system_user_status_id'
		},
		username: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'username'
		},
		password: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'password'
		},
		email: {
			type: DataTypes.JSONB,
			allowNull: true,
			field: 'email'
		},
		phone: {
			type: DataTypes.JSONB,
			allowNull: true,
			field: 'phone'
		},
		address: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'address'
		},
		flagDisabled: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: false,
			field: 'flag_disabled'
		},
		userCreated: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_created'
		},
		userModified: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'user_modified'
		},
		dateTimeCreated: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_created'
		},
		dateTimeModified: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_modified'
		},
		displayName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'display_name'
		},
		firstName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'first_name'
		},
		lastName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'last_name'
		},
		playerLevel: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'player_level'
		},
		age: {
			type: DataTypes.INTEGER,
			allowNull: true,
			field: 'age'
		},
		gender: {
			type: DataTypes.CHAR,
			allowNull: true,
			field: 'gender'
		},
		height: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'height'
		},
		weight: {
			type: DataTypes.REAL,
			allowNull: true,
			field: 'weight'
		},
		dateTimeLatestLogin: {
			type: DataTypes.DATE,
			allowNull: true,
			field: 'date_time_latest_login'
		},
		profileImgUrl: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'profile_img_url'
		},
		flagOauth: {
			type: DataTypes.BOOLEAN,
			allowNull: false,
			defaultValue: false,
			field: 'flag_oauth'
		}		
	}, {
		tableName: 'tbl_master_user'
	});

	tblMasterUser.getUserByUsernameAsync = async function getUserByUsernameAsync(username, attributes = ['uuid']) {
		const [err, user] = await asyncHelper(this.findOne({
			attributes: [...attributes, 'systemUserRoleId'],
			where: {
				username
			}
		}));

		return user;
	} 

	return tblMasterUser;
};
