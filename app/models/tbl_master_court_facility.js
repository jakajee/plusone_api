/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblMasterCourtFacility', {
		masterFacilityUuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			primaryKey: true,
			references: {
				model: 'tbl_master_facility',
				key: 'uuid'
			},
			field: 'master_facility_uuid'
		},
		masterCourtUuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			references: {
				model: 'tbl_master_court',
				key: 'uuid'
			},
			field: 'master_court_uuid'
		}
	}, {
		tableName: 'tbl_master_court_facility'
	});
};
