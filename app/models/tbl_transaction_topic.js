/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('tblTransactionTopic', {
		uuid: {
			type: DataTypes.UUIDV4,
			allowNull: false,
			defaultValue: sequelize.fn('uuid_generate_v4'),
			primaryKey: true,
			field: 'uuid'
		},
		masterSportTypeUuid: {
			type: DataTypes.UUIDV4,
			allowNull: true,
			references: {
				model: 'tbl_master_sport_type',
				key: 'uuid'
			},
			field: 'master_sport_type_uuid'
		},
		topicName: {
			type: DataTypes.STRING,
			allowNull: true,
			field: 'topic_name'
		},
		topicCommentList: {
			type: DataTypes.JSONB,
			allowNull: true,
			field: 'topic_comment_list'
		}
	}, {
		tableName: 'tbl_transaction_topic'
	});
};
