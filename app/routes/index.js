const ResponseModel = require('../core/response_model');
const controllers = require('../controllers/');

module.exports = (app) => {    

    app.get('/api', (req, res) => ResponseModel.send200(res, 'This is Api home'));
    app.get('/api/error', (req, res) => ResponseModel.send500(res, 'sample error'));

    Object.keys(controllers)
        .forEach(ctrlName => {
            controllers[ctrlName](app);
        })
}