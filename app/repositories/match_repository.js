const {
    tblTransactionMatch
} = require('../models');
const asyncHelper = require('../core/async_helper');
const moment = require('moment');

const createMatch = async (model) => {
    const {
        masterCourtUuid,
        matchName,
        matchCapability,
        matchDate,
        matchTimeStart,
        matchTimeEnd,
        matchDescription,
        userCreated,
        masterEventUuid
    } = model;

    const dateTimeCreated = !!matchDate
        ? moment(matchDate).format(Date.momentFormat)
        : Date.now();

    return await asyncHelper(tblTransactionMatch.create({
        masterCourtUuid,
        matchName,
        matchCapability,
        matchTimeStart,
        matchTimeEnd,
        userCreated,
        matchDescription,
        dateTimeCreated,
        masterEventUuid
    }))
}

const getMatchByEventUuid = async masterEventUuid => {
    const [error, match] = await asyncHelper(tblTransactionMatch.findOne({
        where: {
            masterEventUuid
        }
    }));

    return match;
}

module.exports = {
    createMatch,
    getMatchByEventUuid
}