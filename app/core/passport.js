const {
    tblMasterUser
} = require('../models')
const secret = require('../config/secret');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

// Setup options for JwtStrategy
const jwtOption = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Authorization'),
    secretOrKey: secret.jwt
}

const jwtLogin = new JwtStrategy(jwtOption, (payload, done) => {
    const username = payload.sub;
    tblMasterUser.getUserByUsernameAsync(username, ['uuid', 'username'])
        .then(user => {
            if (user) {
                done(null, { ...user.dataValues });
            } else {
                done(null, false);
            }
        })
})

const jwtLoginAdmin = new JwtStrategy(jwtOption, (payload, done) => {
    const username = payload.sub;
    tblMasterUser.getUserByUsernameAsync(username, ['uuid', 'username'])
        .then(user => {
            if (user && user.systemUserRoleId === 1) { // role admin
                done(null, { ...user.dataValues });
            } else {
                done(null, false);
            }
        })
})


module.exports = {
    jwtStrategy: jwtLogin,
    jwtLoginAdmin: jwtLoginAdmin
}