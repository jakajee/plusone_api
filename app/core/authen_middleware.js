const passportService = require('./passport');
const passport = require('passport');

passport.use(passportService.jwtStrategy);
passport.use('admin', passportService.jwtLoginAdmin);

const requireAuth = passport.authenticate('jwt', { session: false });
const requireAuthAdmin = passport.authenticate('admin', { session: false })

module.exports = (req, res, next) => {
    const ignoreClientRoute = ['/login', '/register'];
    const isContainIgnore = ignoreClientRoute.some(r => req.url.indexOf(r) > -1);

    if (req.url.indexOf('/api/public') === -1 && !isContainIgnore) {

        if (req.url.indexOf('/api/datamanagement') === -1) {
            requireAuth(req, res, next);
            return;
        } else {
            requireAuthAdmin(req, res, next);
            return;
        }
    }

    next();
}