module.exports = (err, req, res, next) => {
    return res.status(500).send({
        message: err.message,
        stack: err.stack
    });
}