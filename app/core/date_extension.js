const moment = require('moment');

module.exports = () => {
    Date.momentFormatDate = 'YYYY-MM-DD';
    Date.momentFormatTime = 'HH:mm:ss';
    Date.momentFormat = `${Date.momentFormatDate} ${Date.momentFormatTime}`;
    Date.prototype.momentFormat = Date.momentFormat;

    Date.getStartOfDayDate = () => moment().startOf('day');
    Date.getEndOfDayDate = () => moment().endOf('day');
    Date.getStartOfDayStr = () =>  Date.getStartOfDayDate().format(Date.momentFormat);
    Date.getEndOfDayStr = () => Date.getEndOfDayDate().format(Date.momentFormat);

    Date.prototype.getStartOfDayDate = function() {
        return moment(this).startOf(day);
    }

    Date.prototype.getEndOfDayDate = function() {
        return moment(this).endOf(day);
    }

    Date.prototype.getStartOfDayStr = function() {
        return this.getStartOfDayDate().toString();
    }

    Date.prototype.getEndOfDayStr = function() {
        return this.getEndOfDayDate().toString();
    }

    Date.prototype.getHoursStr = function() {
        return this.toStringFormat('HH:mm');
    }

    Date.prototype.toString = function () {
        return moment(this).format(Date.momentFormat);
    }

    Date.prototype.toJSON = function() {
        return moment(this).format(this.momentFormat);
    }

    Date.prototype.toStringFormat = function(format) {
        return moment(this).format(format || Date.momentFormat);
    }

    Date.prototype.toStringDate = function() {
        return moment(this).format('YYYY-MM-DD');
    }

}