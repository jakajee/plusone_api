class ResponseModel {

    constructor(flagSuccess = false, message = '', data = null) {
        this.flagSuccess = flagSuccess;
        this.message = message;
        this.data = data;
    }

    static send200(res, message) {
        return res.status(200).send(new ResponseModel(true, message));
    }

    static send500(res, error) {
        return res.status(500).send(new ResponseModel(false, error));
    }
    
    sendResponse(res) {
        return res.status(this.flagSuccess ? 200 : 500).send(this);
    }
}

module.exports = ResponseModel;