const bcrypt = require('bcryptjs');

module.exports = {
    async generateHash(value) {
        const hashRound = Math.ceil(Math.random() * 10);
        const salt = await bcrypt.genSalt(hashRound);
        const hash = await bcrypt.hash(value, salt);
        
        return hash;
    },

    async isHashMatch(value, hash) {
        const match = await bcrypt.compare(value, hash);
        return match;
    }
}