const jwt = require('jwt-simple');
const secret = require('../../config/secret');

module.exports = {
    getToken(user) {
        // issue at time
        const iat = new Date().getTime();

        return jwt.encode({
            sub: user.username,
            iat
        }, secret.jwt)
    }
}