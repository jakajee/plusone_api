const {
    tblMasterCourt,
    tblMasterSportType,
    tblMasterEvent,
    sequelize
} = require('../../../models');
const Op = sequelize.Op;
const _ = require('lodash');
const asyncHelper = require('../../../core/async_helper');
const matchRepository = require('../../../repositories/match_repository');

module.exports = app => {
    app
        // get event list
        .get('/api/v1/event', async (req, res) => {
            const response = await getEventList(req);
            return res.send(response);
        })
        // get event by eventId (event_id)
        .get('/api/v1/event/:eventId', async (req, res) => {
            const { eventId } = req.params;
            const response = await getEventByEventId(eventId);
            if (response) {
                const match = await matchRepository.getMatchByEventUuid(response.uuid);

                _.assign(response, {
                    matchCapability: match.matchCapability,
                    matchUuid: match.uuid
                })
            }

            return res.send(_.omit(response, ['uuid']));
        });


    const getEventByEventId = async eventId => {
        const [errGetEvent, eventModel] = await asyncHelper(tblMasterEvent.findOne({
            where: {
                eventId
            },
            include: [{
                model: tblMasterSportType,
                attributes: ['sportId', 'sportName']
            }, {
                model: tblMasterCourt,
                attributes: ['courtName', 'courtLat', 'courtLng']
            }]
        }))

        if (errGetEvent)
            return null;

        return _.omit(eventModel.get({ plain: true }), ['flagDisabled', 'userCreated', 'userModified', 'dateTimeCreated', 'dateTimeModified']);
    }

    const getEventList = async (req) => {
        const {
            eventDateTimeStart,
            eventDateTimeEnd
        } = req.query;

        const [errGetEvent, resultEventList] = await asyncHelper(tblMasterEvent.findAll({
            where: {
                eventDateTimeStart: {
                    [Op.gte]: eventDateTimeStart
                },
                eventDateTimeEnd: {
                    [Op.lte]: eventDateTimeEnd
                }
            },
            attributes: ['eventId', 'eventName','eventDateTimeStart', 'eventDateTimeEnd', 'eventImg'],
            include: [{
                model: tblMasterSportType,
                attributes: ['sportId']
            }],
            raw: true
        }));

        if (resultEventList && resultEventList.length > 0) {
            const result = resultEventList.map(data => ({
                ..._.omit(data, ['tblMasterSportType.sportId']),
                sportId: data['tblMasterSportType.sportId']
            }));

            return result;
        }

        return [];
    }
}