const {
    tblTransactionMatch,
    tblTransactionMatchUser,
    tblMasterUser,
    tblMasterEvent,
    sequelize
} = require('../../../models');
const asyncHelper = require('../../../core/async_helper');
const ResponseModel = require('../../../core/response_model');
const _ = require('lodash-uuid');
const moment = require('moment');
const Op = sequelize.Op;
const matchRepository = require('../../../repositories/match_repository');

module.exports = (app) => {
    app
        .get('/api/v1/match/list/:courtId/:date', async ({ params: { courtId, date } }, res) => {
            const data = await getMatchList(courtId, date);

            return res.status(200).send(data);
        })

        .get('/api/v1/match/:matchId', async (req, res) => {
            const data = await getMatchById(req.params.matchId);

            return res.status(200).send(data);
        })

        .post('/api/v1/match/create', async (req, res) => {
            const {
                matchUserList
            } = req.body;
            let error, match;

            [error, match] = await createMatch(req);

            if (error) return res.status(500).send(error);

            [error, result] = await createUserInMatch(match.uuid, matchUserList);

            if (error) {
                result = await match.destroy();
                return res.status(500).send(new ResponseModel(false, 'Cannot Create Match'));
            }

            return res.status(200).send(match);
        })

        .patch('/api/v1/match/join', async (req, res) => {
            const {
                matchUuid,
                masterUserUuid
            } = req.body;

            await joinMatch(matchUuid, masterUserUuid);
            const match = await getMatchById(matchUuid);

            return res.status(200).send(match);

        });

    async function createMatch(req) {
        return await matchRepository.createMatch(req);
    }

    async function createMatch(req) {
        const {
            masterCourtUuid,
            matchName,
            matchCapability,
            matchDate,
            matchTimeStart,
            matchTimeEnd,
            matchDescription,
            userCreated
        } = req.body;

        const dateTimeCreated = !!matchDate
            ? moment(matchDate).format(Date.momentFormat)
            : Date.now();

        return await asyncHelper(tblTransactionMatch.create({
            masterCourtUuid,
            matchName,
            matchCapability,
            matchTimeStart,
            matchTimeEnd,
            userCreated,
            matchDescription,
            dateTimeCreated: dateTimeCreated
        }))
    }

    async function getMatchById(transactionMatchUuid) {
        const [error, data] = await asyncHelper(tblTransactionMatch.findByPk(transactionMatchUuid, {
            include: [{
                model: tblTransactionMatchUser,
                attributes: ['masterUserUuid', 'dateTimeCreated'],
                include: [{
                    model: tblMasterUser,
                    attributes: ['username', 'displayName', 'playerLevel', 'profileImgUrl'],
                    required: false
                }]
            }],
            order: [[tblTransactionMatchUser, 'dateTimeCreated', 'ASC']]
        }));

        return data;
    }

    async function getMatchList(courtId, date) {
        const dateFromStr = moment(date).startOf('day').format(Date.momentFormat);
        const dateToStr = moment(date).endOf('day').format(Date.momentFormat);
        const [error, data] = await asyncHelper(tblTransactionMatch.findAll({
            where: {
                dateTimeCreated: {
                    [Op.between]: [dateFromStr, dateToStr]
                },
                masterCourtUuid: {
                    [Op.eq]: courtId
                }
            },
            attributes: [
                'uuid',
                'matchName',
                'matchTimeStart',
                'matchTimeEnd',
                'matchCapability',
                'matchDescription',
                'dateTimeCreated'
            ],
            include: [{
                model: tblTransactionMatchUser,
                attributes: ['masterUserUuid'],
                include: [{
                    model: tblMasterUser,
                    attributes: ['username', 'displayName'],
                    required: false
                }]
            }, {
                model: tblMasterEvent,
                attributes: ['eventId', 'eventName'],
                required: false
            }]
        }));

        return !!error ? [] : data;

    }

    async function createUserInMatch(transactionMatchUuid, matchUserList) {
        return await asyncHelper(
            tblTransactionMatchUser
                .bulkCreate(
                    matchUserList.map(item => ({
                        ...item,
                        transactionMatchUuid,
                        dateTimeCreated: new Date()
                    }))
                ));

    }

    async function joinMatch(transactionMatchUuid, masterUserUuid) {
        const [error, data] = await asyncHelper(
            tblTransactionMatchUser.create({
                transactionMatchUuid,
                masterUserUuid,
                dateTimeCreated: new Date()
            })
        );

        return error
            ? new ResponseModel(false, 'Join match failed')
            : new ResponseModel(true, 'Join match success');
    }
}