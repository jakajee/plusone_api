const {
    tblMasterUser
} = require('../../../models')
const ResponseModel = require('../../../core/response_model');
const asyncHelper = require('../../../core/async_helper');
const hashHelper = require('../../infrastructure/hash_helper');
const tokenHelper = require('../../infrastructure/token_helper');

module.exports = (app) => {

    app
        .post('/api/public/v1/user/sociallogin', async (req, res) => {
            const {
                username
            } = req.body;

            const flagDuplicateUser = await hasUser(username);

            if (!flagDuplicateUser) {
                // username does not exists - create new user
                const resultCreate = await createUser(req);
                return resultCreate.sendResponse(res);
            } else {
                // username exists - create token
                const [err, user] = await asyncHelper(
                    tblMasterUser.findOne({
                        attributes: ['uuid', 'username', 'systemUserRoleId', 'displayName', 'dateTimeLatestLogin', 'dateTimeModified'],
                        where: {
                            username,
                            flagDisabled: false
                        }
                    })
                )
                
                updateUserInfoFromFB(user, req);
                user.dateTimeLatestLogin = new Date();
                user.dateTimeModified = new Date();
                await user.save();

                const token = tokenHelper.getToken({
                    username
                });

                const responseModel = new ResponseModel(true, 'Login success', {
                    token,
                    username,
                    displayName: user.displayName
                });

                return responseModel.sendResponse(res);
            }
        })

        .post('/api/public/v1/user/register', async (req, res) => {
            const {
                username,
                email,
                password,
            } = req.body

            if (!username || !password)
                return ResponseModel.send500(res, 'Please Input username and pssword');

            const flagDuplicateUser = await hasUser(username);

            if (flagDuplicateUser)
                return ResponseModel.send500(res, 'Duplicate user, please try again');

            const result = await createUser(req);

            return result.sendResponse(res);
        })

        .post('/api/public/v1/user/login', async (req, res) => {
            const result = await doLogin(req);

            return result.sendResponse(res);
        })

        .get('/api/v1/user/:username', async (req, res) => {
            const {
                username
            } = req.params;

            const user = await tblMasterUser.findOne({
                attributes: {
                    exclude: ['password']
                },
                where: {
                    username
                }
            });

            return res.status(200).send(user);
        })

    function updateUserInfoFromFB(user, req) {
        const {
            firstName,
            lastName,
            displayName,
            phone,
            address,
            playerLevel,
            age,
            gender,
            height,
            weight,
            userCreated,
            flagOauth,
            profileImgUrl
        } = req.body;

        // update user information from facebook 
        Object.assign(user, {
            firstName,
            lastName,
            displayName,
            phone,
            address,
            playerLevel,
            age,
            gender,
            height,
            weight,
            userCreated,
            flagOauth,
            profileImgUrl
        })
    }

    async function hasUser(username) {
        const data = await tblMasterUser.getUserByUsernameAsync(username);
        return !data ? false : true;
    }


    async function createUser(req) {
        const {
            username,
            password,
            firstName,
            lastName,
            displayName,
            email,
            phone,
            address,
            playerLevel,
            age,
            gender,
            height,
            weight,
            userCreated,
            flagOauth,
            profileImgUrl
        } = req.body

        const hashedPassword = await hashHelper.generateHash(password);

        const [err, user] = await asyncHelper(tblMasterUser
            .create({
                username,
                password: hashedPassword,
                firstName,
                lastName,
                displayName,
                email: !email ? null : [email],
                phone: !phone ? null : [phone],
                address,
                playerLevel,
                userCreated,
                age,
                gender,
                height,
                weight,
                userCreated,
                dateTimeCreated: new Date(),
                flagOauth,
                profileImgUrl
            }));

        return !!err
            ? new ResponseModel(false, 'Create user failed')
            : new ResponseModel(true, 'Create user success', {
                token: tokenHelper.getToken(user),
                username,
                displayName
            });
    }

    async function doLogin(req) {
        const {
            username,
            password
        } = req.body;

        const [err, user] = await asyncHelper(
            tblMasterUser.findOne({
                attributes: ['uuid', 'username', 'password', 'displayName', 'dateTimeLatestLogin', 'systemUserRoleId'],
                where: {
                    username,
                    flagDisabled: false
                }
            })
        )

        if (!user) {
            return new ResponseModel(false, 'Incorrect username or password');
        } else {
            const match = await hashHelper.isHashMatch(password, user.password);

            if (!match) return new ResponseModel(false, 'Incorrect username or password');

            user.dateTimeLatestLogin = new Date();
            user.dateTimeModified = new Date();
            await user.save();

            const token = tokenHelper.getToken(user);

            return new ResponseModel(true, 'Login success', {
                token,
                username,
                displayName: user.displayName,
                systemRoleId: user.systemUserRoleId
            });
        }
    }


}
