const {
    tblTransactionReview,
    tblMasterUser,
    sequelize: { Op }
} = require('../../../models');
const asyncHelper = require('../../../core/async_helper');
const ResponseModel = require('../../../core/response_model');

module.exports = (app) => {
    app
        .post('/api/v1/review/create', async (req, res) => {
            const result = await createReview(req);

            return result.sendResponse(res);
        })

        .patch('/api/v1/review/update', async (req, res) => {
            const result = await updateReview(req);

            return result.sendResponse(res);
        })

        .get('/api/v1/review/validate/:courtId/:userCreated', async (req, res) => {
            const {
                courtId,
                userCreated
            } = req.params;

            const result = await canCreateReview(userCreated, courtId);

            return res.status(200).send(result);
        })

        .get('/api/v1/review/:courtId', async (req, res) => {
            const {
                courtId
            } = req.params;

            const reviewList = await getReviewList(courtId);

            return res.status(200).send(reviewList);
        })

    async function getReviewList(courtId) {
        return await tblTransactionReview.findAll({
            where: {
                masterCourtUuid: courtId
            },
            order: [
                ['dateTimeCreated', 'DESC']
            ]
        });
    }

    async function createReview(req) {
        const {
            reviewRating,
            reviewDescription,
            userCreated,
            masterCourtUuid
        } = req.body;

        const validReviewer = await canCreateReview(userCreated, masterCourtUuid);

        if (validReviewer) {
            const user = await tblMasterUser.findOne({
                attributes: ['uuid'],
                where: {
                    username: userCreated,
                    flagDisabled: false
                }
            });

            const [err, review] = await asyncHelper(
                tblTransactionReview
                    .create({
                        reviewRating,
                        reviewDescription,
                        reviewDate: new Date().toStringDate(),
                        userCreated,
                        dateTimeCreated: new Date(),
                        masterCourtUuid,
                        masterUserUuid: user.uuid,
                    })
            );

            if (!review) return new ResponseModel(false, `Create review failed: ${err}`);

            return new ResponseModel(true, 'Create review success', review);
        } else {
            return new ResponseModel(false, 'This user already add a review today');
        }
    }

    async function updateReview(req) {
        const {
            reviewId,
            reviewDescription
        } = req.body;

        let [err, review] = await asyncHelper(tblTransactionReview.findByPk(reviewId));

        if (!review) return new ResponseModel(false, 'Review not found');

        review.reviewDescription = reviewDescription;
        review.dateTimeModified = Date.now();

        [err, review] = await asyncHelper(review.save());

        if (!review) return new ResponseModel(false, 'Update review failed');

        return new ResponseModel(true, 'Update review success');
    }

    // reviewer cannot review court more than once in the sameday
    async function canCreateReview(userCreated, courtId) {
        const review = await tblTransactionReview.findOne({
            attributes: ['uuid'],
            where: {
                userCreated,
                masterCourtUuid: courtId,
                reviewDate: new Date().toStringDate()
            }
        });

        return !review;
    }
}