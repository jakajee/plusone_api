const {
    tblMasterSportType
} = require('../../../models');
const asyncHelper = require('../../../core/async_helper');
const ResponseModel = require('../../../core/response_model');

module.exports = (app) => {
    app
        .get('/api/v1/sport/list', async (req, res) => {
            const sportList = await getSportTypeList();

            return res.status(200).send(sportList);
        })


    async function getSportTypeList() {
        const [error, sportList] = await asyncHelper(tblMasterSportType.findAll({
            attributes: ['uuid', 'sportName', 'imgUrl', 'cssIcon', 'sportId'],
            order: ['sportName']
        }));

        return sportList;
    }
}