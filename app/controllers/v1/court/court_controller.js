const {
    tblMasterCourt,
    tblMasterSportType,
    tblMasterFacility,
    tblTransactionMatch,
    tblTransactionMatchUser,
    tblTransactionReview,
    tblMasterUser,
    sequelize
} = require('../../../models');
const moment = require('moment');
const asyncHelper = require('../../../core/async_helper');
const ResponseModel = require('../../../core/response_model');
const Op = sequelize.Op;


module.exports = (app) => {

    app
        .get('/api/v1/court/search', async (req, res) => {
            const {
                courtName,
                sportId
            } = req.query;

            const [error, data] = await asyncHelper(tblMasterCourt.spSearchCourt([courtName, sportId]));

            return res.status(!!error ? 500 : 200).send(data || new ResponseModel('Not found any records'));
        })

        .get('/api/v1/court/:courtId', async (req, res) => {
            const {
                courtId                
            } = req.params;

            const {
                date
            } = req.query;            

            const dateFromStr = moment(date).startOf('day').format(Date.momentFormat);
            const dateToStr = moment(date).endOf('day').format(Date.momentFormat);

            const [error, court] = await asyncHelper(tblMasterCourt.findByPk(courtId, {
                include: [{
                    model: tblMasterSportType
                }, {
                    model: tblMasterFacility,
                    attributes: [
                        'facilityName',
                        'facilityIcon'
                    ]
                }, {
                    model: tblTransactionMatch,
                    where: {
                        dateTimeCreated: {
                            [Op.between]: [dateFromStr, dateToStr]
                        }
                    },
                    required: false,
                    attributes: [
                        'uuid',
                        'matchName',
                        'matchTimeStart',
                        'matchTimeEnd',
                        'matchCapability',
                        'matchDescription',
                        'dateTimeCreated'
                    ],
                    include: [{
                        model: tblTransactionMatchUser,
                        attributes: ['masterUserUuid'],
                        include: [{
                            model: tblMasterUser,
                            attributes: ['username', 'displayName'],
                            required: false
                        }]
                    }]
                }, {
                    model: tblTransactionReview,
                    attributes: ['reviewRating']
                }],
                order: [
                    [tblTransactionMatch, 'dateTimeCreated', 'ASC'],
                    [tblTransactionMatch, 'matchTimeStart', 'ASC']
                ] 
            }));

            if (error) {
                console.log(error);
                return res.status(500).send(new ResponseModel(false, 'Court Not Found'))
            }

            court.dataValues.courtRating = court.getCourtRating();

            return res.status(200).send(court);
        })

}