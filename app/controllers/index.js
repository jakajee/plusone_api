const v1_courtController = require('./v1/court/court_controller');
const v1_matchController = require('./v1/match/match_controller');
const v1_userController = require('./v1/user/user_controller');
const v1_reviewController = require('./v1/review/review_controller');
const v1_sportController = require('./v1/sport/sport_controller');
const v1_eventController = require('./v1/event/event_controller');

const court_manager_controller = require('./administration/court_manager_controller');
const event_manager_controller = require('./administration/event_manager_controller');
const system_data_controller = require('./administration/system_data_controller');
const master_data_controller = require('./administration/master_data_controller');

module.exports = {
    v1_courtController,
    v1_matchController,
    v1_userController,
    v1_reviewController,
    v1_sportController,
    v1_eventController,
    court_manager_controller,
    event_manager_controller,
    system_data_controller,
    master_data_controller
}