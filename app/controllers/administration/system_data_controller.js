const {
    tblMasterCourt,
    tblSystemProvince,
    tblSystemArea,
    tblMasterSportType,
    tblMasterFacility,
    tblMasterCourtFacility,
    sequelize
} = require('../../models');
const moment = require('moment');
const asyncHelper = require('../../core/async_helper');
const ResponseModel = require('../../core/response_model');
const Op = sequelize.Op;

module.exports = (app) => {

    app
        .get('/api/datamanagement/system/province', handlerSystem(() => tblSystemProvince.findAll()))

        .get('/api/datamanagement/system/area', handlerSystem(req => tblSystemArea.findAll({
            where: {
                systemProvinceId: req.query.systemProvinceId
            },
            order: ['areaName']
        })))

    function handlerSystem(asyncFn) {
        return async (req, res) => {
            const [err, data] = await asyncHelper(asyncFn(req));

            return res.status(200).send(data);
        }
    }
}