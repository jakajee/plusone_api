const {
    tblMasterCourt,
    tblSystemProvince,
    tblSystemArea,
    tblMasterSportType,
    tblMasterFacility,
    tblMasterCourtFacility,
    sequelize
} = require('../../models');
const moment = require('moment');
const asyncHelper = require('../../core/async_helper');
const ResponseModel = require('../../core/response_model');
const Op = sequelize.Op;

module.exports = (app) => {

    app
        .get('/api/datamanagement/courtmanager', async (req, res) => {
            const {
                courtName = '',
                sportTypeId
            } = req.query

            const wherClause = {
                courtName: {
                    [Op.like]: `%${courtName}%`
                }
            }

            if (!!sportTypeId)
                wherClause.masterSportTypeUuid = {
                    [Op.eq]: sportTypeId
                }

            const [err, courtList] = await asyncHelper(tblMasterCourt.findAll({
                where: wherClause,
                order: [
                    ['courtName', 'asc']
                ],
                attributes: [
                    'uuid',
                    'courtName',
                    'flagDisabled'
                ],
                include: [{
                    model: tblSystemProvince,
                    attributes: ['provinceId', 'provinceName']
                }, {
                    model: tblSystemArea,
                    attributes: ['areaId', 'areaName'],
                    required: false
                }, {
                    model: tblMasterSportType,
                    attributes: ['sportName']
                }]

            }));

            if (!!err) {
                console.log(err);
                return new ResponseModel(false, 'Court not found').sendResponse(res);
            }

            return res.status(200).send(courtList);
        })
        .get('/api/datamanagement/courtmanager/:courtId', async (req, res) => {
            const {
                courtId
            } = req.params;

            const [err, courtDetail] = await asyncHelper(tblMasterCourt.findByPk(courtId, {
                include: [{
                    model: tblMasterFacility,
                    attributes: [
                        'facilityName',
                        'facilityIcon'
                    ]
                }],
                attributes: [
                    'uuid',
                    'courtName',
                    'courtDescription',
                    'courtLat',
                    'courtLng',
                    'courtImageList',
                    'flagDisabled',
                    'systemProvinceId',
                    'systemAreaId',
                    'masterSportTypeUuid',
                    'masterPlaceUuid',
                    'courtOpenHours',
                    'courtCloseHours',
                    'courtCount'
                ]
            }));

            const [errFacility, masterFacilities] = await asyncHelper(tblMasterFacility.findAll({
                attributes: ['uuid', 'facilityName']
            }))

            extractTime(courtDetail, 'courtOpenHours');
            extractTime(courtDetail, 'courtCloseHours')

            courtDetail.dataValues.facilityList = masterFacilities.map(data => ({
                value: data.uuid,
                text: data.facilityName,
                checked: courtDetail.tblMasterFacilities.some(fc => fc.tblMasterCourtFacility.masterFacilityUuid === data.uuid)
            }))

            return res.send(courtDetail);
        })
        .patch('/api/datamanagement/courtmanager/:courtId', async (req, res) => {
            const result = await updateCourt(req);

            return result.sendResponse(res);
        })
        .post('/api/datamanagement/courtmanager', async (req, res) => {
            const result = await createCourt(req);

            return result.sendResponse(res);
        })

        .delete('/api/datamanagement/courtmanager/:courtId', async (req, res) => {
            const { courtId } = req.params;
            const [errGetCourt, courtModel] = await asyncHelper(tblMasterCourt.findByPk(courtId));

            if(errGetCourt)
                return new ResponseModel(false, "Court Not Found").sendResponse(res);
            
            courtModel.flagDisabled = !courtModel.flagDisabled;

            const [errSave, result] = await asyncHelper(courtModel.save());

            if(errSave)
                return new ResponseModel(false, 'Toggle Disable Failed').sendResponse(res);
            
            return new ResponseModel(true, 'Toggle Disable Success', result).sendResponse(res);
        })

    async function updateCourt(req) {
        const {
            courtId
        } = req.params;

        const {
            courtName,
            courtDescription,
            courtLat,
            courtLng,
            courtImageList,
            courtOpenHours,
            courtCloseHours,
            courtCount,
            flagDisabled = false,
            systemProvinceId,
            systemAreaId,
            masterPlaceUuid,
            masterSportTypeUuid,
            facilityList
        } = req.body;

        const [err, courtDetail] = await asyncHelper(tblMasterCourt.findByPk(courtId, {
            include: [{
                model: tblMasterFacility
            }]
        }));

        Object.assign(courtDetail, {
            courtName,
            courtDescription,
            courtLat,
            courtLng,
            courtImageList,
            courtOpenHours,
            courtCloseHours,
            courtCount,
            flagDisabled,
            systemProvinceId,
            systemAreaId,
            masterPlaceUuid,
            masterSportTypeUuid
        });

        let [errRemove, resultRemove] = await asyncHelper(courtDetail.removeTblMasterFacilities(courtDetail.tblMasterFacilities));
        let result = await addFacility(facilityList, courtId);

        await courtDetail.save();

        if (!!err) {
            return new ResponseModel(false, 'Update failed, please try again later');
        }

        return new ResponseModel(true, 'Update success');
    }

    async function createCourt(req) {
        const {
            courtName,
            courtDescription,
            courtLat,
            courtLng,
            courtImageList,
            courtOpenHours,
            courtCloseHours,
            courtCount,
            flagDisabled = false,
            systemProvinceId,
            systemAreaId,
            masterPlaceUuid,
            masterSportTypeUuid,
            facilityList
        } = req.body;

        let [err, createResult] = await asyncHelper(tblMasterCourt.create({
            courtName,
            courtDescription,
            courtLat,
            courtLng,
            courtImageList,
            courtOpenHours,
            courtCloseHours,
            courtCount,
            flagDisabled,
            systemProvinceId,
            systemAreaId,
            masterPlaceUuid,
            masterSportTypeUuid
        }))

        if (!!err) {
            return new ResponseModel(false, 'Create failed, please try again later');
        }

        let [errAdd, resultAdd] = await addFacility(facilityList, createResult.uuid);

        return new ResponseModel(true, 'Create success');
    }

    async function addFacility(facilityList, courtUuid) {
        return await asyncHelper(
            tblMasterCourtFacility.bulkCreate(facilityList.map(facilityUuid => ({
                masterCourtUuid: courtUuid,
                masterFacilityUuid: facilityUuid
            })))
        )
    }

    function extractTime(courtModel, propName) {
        if (!!courtModel[propName])
            courtModel[propName] = courtModel[propName].substr(0, 5);
    }
}