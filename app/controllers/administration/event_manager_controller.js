const {
    tblMasterCourt,
    tblMasterSportType,
    tblMasterEvent,
    sequelize
} = require('../../models');
const _ = require('lodash');
const moment = require('moment');
const shortId = require('shortid');
const asyncHelper = require('../../core/async_helper');
const ResponseModel = require('../../core/response_model');
const Op = sequelize.Op;
const matchRepository = require('../../repositories/match_repository');

module.exports = app => {
    app
        .get('/api/datamanagement/eventmanager', async (req, res) => {
            // do get list
            const [err, eventList] = await asyncHelper(getEventList(req.query.eventName));
            return res.send(eventList);
        })

        .get('/api/datamanagement/eventmanager/:uuid', async (req, res) => {
            // do get by id
            const { uuid } = req.params;
            const [err, eventModel] = await asyncHelper(getEventModelById(uuid));
            const [errGetMatch, matchModel] = await asyncHelper(matchRepository.getMatchByEventUuid(uuid));

            const plainEventModel = eventModel.get({ plain: true });
            plainEventModel.matchCapability = matchModel.matchCapability;

            return res.send(plainEventModel)
        })

        .post('/api/datamanagement/eventmanager', async (req, res) => {
            // do create
            const result = await createEvent(req);
            return result.sendResponse(res);
        })

        .patch('/api/datamanagement/eventmanager/:eventId', async (req, res) => {
            // do update
            const result = await updateEvent(req);
            return result.sendResponse(res);
        })

        .delete('/api/datamanagement/eventmanager/:eventId', async (req, res) => {
            const { eventId } = req.params;
            const [err, eventModel] = await asyncHelper(getEventModelById(eventId));

            console.log(eventModel);

            eventModel.flagDisabled = !eventModel.flagDisabled;

            let [errUpdate, updateResult] = await asyncHelper(eventModel.save());

            if(errUpdate)
                return new ResponseModel(false, errUpdate).sendResponse(res);

            return new ResponseModel(true, "Toggle Disable Success", updateResult).sendResponse(res);
        })

    const createEvent = async req => {
        const newEvent = getEventModelFromRequest(req);

        let [error, result] = await asyncHelper(tblMasterEvent.create({
            ...newEvent,
            eventId: shortId.generate(),
            dateTimeCreated: Date.now()
        }));

        if (!!error)
            return new ResponseModel(false, 'Create failed, please try again later')

        // create match
        newEvent.uuid = result.uuid;
        const resultCreateMatch = await createMatchEvent(req, newEvent);

        if (!resultCreateMatch) {
            let [errRemoveEvent, response] = await asyncHelper(tblMasterEvent.destroy({
                where: {
                    uuid: newEvent.uuid
                }
            }));

            return new Response(false, 'Create failed');
        }

        return new ResponseModel(true, 'Create success', result);
    }

    const updateEvent = async req => {
        const updatedEvent = getEventModelFromRequest(req);
        const eventId = req.params.eventId;

        let [errGet, event] = await asyncHelper(getEventModelById(eventId));

        const oldCourtUuid = event.masterCourtUuid;

        _.assign(event, {
            ...updatedEvent,
            dateTimeModified: Date.now()
        });

        let transaction = await sequelize.transaction();

        let [errUpdate, updateResult] = await asyncHelper(event.save({ transaction }));
        let resultUpdateCourt = await updateCourtInEvent(eventId, req.body.matchCapability, event.masterCourtUuid, oldCourtUuid, transaction);

        if (!!errUpdate || !resultUpdateCourt) {
            await transaction.rollback();
            return new ResponseModel(false, 'Update failed, please try again later');
        }

        await transaction.commit();

        return new ResponseModel(true, 'Update success', updateResult);
    }

    const updateCourtInEvent = async (eventUuid, matchCapability, newCourtUuid, oldCourtUuid, transaction) => {
        try {
            let matchModel = await matchRepository.getMatchByEventUuid(eventUuid);

            matchModel.matchCapability = matchCapability || matchModel.matchCapability;
            matchModel.masterCourtUuid = newCourtUuid !== oldCourtUuid
                ? newCourtUuid
                : oldCourtUuid;
            await matchModel.save({ transaction });

            return true;
        } catch (err) {
            console.log(err);
            return false;
        }
    }

    const getEventModelFromRequest = req => {
        const {
            eventName,
            eventDescription,
            eventReward,
            eventFee,
            eventDateTimeStart,
            eventDateTimeEnd,
            eventImg,
            userCreated,
            userModified,
            masterSportTypeUuid,
            masterCourtUuid
        } = req.body;

        return {
            eventName,
            eventDescription,
            eventReward,
            eventFee,
            eventDateTimeStart,
            eventDateTimeEnd,
            eventImg,
            userCreated,
            userModified,
            masterSportTypeUuid,
            masterCourtUuid
        }
    }

    const getEventModelById = async uuid => await tblMasterEvent.findByPk(uuid, {
        include: [{
            model: tblMasterCourt,
            attributes: ['courtName']
        }]
    });

    const getEventList = async eventName => {
        return await tblMasterEvent.findAll({
            where: {
                eventName: {
                    [Op.like]: `%${eventName}%`
                }
            },
            attributes: ['uuid', 'eventName', 'flagDisabled', 'eventDateTimeStart', 'eventDateTimeEnd', 'dateTimeCreated', 'dateTimeModified'],
            include: [{
                model: tblMasterSportType,
                attributes: ['sportName']
            }, {
                model: tblMasterCourt,
                attributes: ['courtName']
            }],
            order: [['dateTimeCreated', 'DESC']]
        })
    }

    const createMatchEvent = async (req, event) => {
        // create match
        const dateStart = moment(event.eventDateTimeStart);
        const dateEnd = moment(event.eventDateTimeEnd);

        let [errCreateMatch, resultCreateMatch] = await asyncHelper(matchRepository.createMatch({
            masterCourtUuid: event.masterCourtUuid,
            matchName: event.eventName,
            matchCapability: req.body.matchCapability,
            matchDate: dateStart.format(Date.momentFormatDate),
            matchTimeStart: dateStart.format(Date.momentFormatTime),
            matchTimeEnd: dateEnd.format(Date.momentFormatTime),
            matchDescription: event.eventDescription,
            dateTimeCreated: event.dateTimeCreated,
            userCreated: event.userCreated,
            masterEventUuid: event.uuid
        }));

        return !errCreateMatch
    }
}