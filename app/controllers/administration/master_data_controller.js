const {
    tblSystemProvince,
    tblSystemArea,
    tblMasterSportType,
    tblMasterFacility,
    tblMasterPlace,
    tblMasterCourt,
    sequelize
} = require('../../models');
const moment = require('moment');
const asyncHelper = require('../../core/async_helper');
const ResponseModel = require('../../core/response_model');
const Op = sequelize.Op;

module.exports = (app) => {

    app

        .get('/api/datamanagement/master/facility', handlerGetMaster(() => tblMasterFacility.findAll({
            attributes: ['uuid', 'facilityName', 'facilityIcon', 'facilityName']
        })))

        .get('/api/datamanagement/master/sport', handlerGetMaster(() => tblMasterSportType.findAll({
            attributes: ['uuid', 'sportName', 'imgUrl', 'cssIcon'],
            order: ['sportName']
        })))

        .get('/api/datamanagement/master/places', handlerGetMaster(() => tblMasterPlace.findAll({
            where: {
                flagDisabled: false
            }
        })))

        .get('/api/datamanagement/master/courts', handlerGetMaster(req => tblMasterCourt.findAll({
            where: {
                masterSportTypeUuid: req.query.sportTypeUuid,
                courtName: {
                    [Op.like]: `%${req.query.courtName}%`
                }
            },
            attributes: ['uuid', 'courtName'],
            order: ['courtName']
        })))

        .post('/api/datamanagement/master/place', async (req, res) => {
            const {
                placeName,
                placeDescription,
                userCreated
            } = req.body;

            const [err, placeModel] = await asyncHelper(tblMasterPlace.create({
                placeName,
                placeDescription,
                userCreated,
                dateTimeCreated: new Date()
            }));

            if(err)
                return res.status(500).send(err);

            return res.status(200).send({
                placeId: placeModel.uuid
            })
        })

    function handlerGetMaster(asyncFn) {
        return async (req, res) => {
            const [err, data] = await asyncHelper(asyncFn(req));

            return res.status(200).send(data);
        }
    }

}